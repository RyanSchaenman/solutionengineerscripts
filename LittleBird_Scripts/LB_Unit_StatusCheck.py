# This is a script that will allow Little Bird to use an ICCID to generate a report to check
# the online status of a specific SIM. This will indicate whether a specific Ethernet Bridge is online

# Importing packages
import http.client
import json
import csv
import argparse

# Application Settings
application_name = "Verify Online Status of UE"
application_version = "1.3.0"
application_description = "The %s is to verify the Online Status of a UE for a given Unit at the " \
                          "Little Bird Happy Valley Property." % application_name

# This API Call gets SIM Related Details on the provided ICCID
def get_SIM_information(iccid, apikey):
    conn = http.client.HTTPSConnection("api.alefedge.com")

    headers = {'accept': "application/json"}

    request_string = '/connectivity/v1/sim?apikey=' + apikey + '&iccid=' + iccid

    conn.request("GET", request_string, headers=headers)

    res = conn.getresponse()
    data = res.read()

    print(data.decode("utf-8"))
    return data.decode("utf-8")

# This API will segment the response of Get SIM Information to obtain the IMSI of the specific SIM
def parse_SIM_details(SIM_Info_response):
    split_response = SIM_Info_response.split('"')
    return split_response[19]


# This function will call the Check Online UE Status for the particular SIM that is being inquired upon
def check_online_status(IMSI, api__key):
    conn = http.client.HTTPSConnection("api.alefedge.com")

    payload = '{\"start_date_time\":\"NOW\",\"end_date_time\":\"NOW\",\"dimensions\":[\"enodeb_ip\",\"ue_ip\",\"lr_id\"],\"dimension_filter_clauses\":[{\"filters\":[{\"dimension_name\":\"account_id\",\"operator\":\"EXACT\",\"expressions\":[\"635fc7e15d9055448d401f34\"]},{\"dimension_name\":\"imsi\",\"operator\":\"EXACT\",\"expressions\":[\"' + IMSI + '\"]}]}],\"metrics\":[{\"name\":\"ue_online_status\",\"aggregation_type\":\"EXISTS\"}]}'

    headers = {
        'accept': "application/json",
        'content-type': "application/json"
    }

    request_string = "/connectivity/v1/report?apikey=" + api__key

    conn.request("POST", request_string , payload, headers)

    res = conn.getresponse()
    data = res.read()

    print(data.decode("utf-8"))
    return data.decode("utf-8")

def unit_check(csv_name, unit, api_key):
    with open(csv_name, 'r') as file:
        csvreader = csv.reader(file)
        for row in csvreader:
            if row[0] == unit:
                imsi = row[3]

    check_online_status(imsi, api_key)

def cli_parser():
    # This function creates a parser for CLI arguments

    parser = argparse.ArgumentParser(description=application_description)
    parser.add_argument('-u', '--unit_number', required=True, default=None, help="Provide Unit Number that you would like to check online status of.")
    parser.add_argument('-a', '--API_Key', required=False, default='FPgyCNX0wiJqKHAZKJNONlfMSlHcYCta', help="Provide the Alef API Key for your account.")
    parser.add_argument('-f', '--File_Name', required=False, default='LB_Deployed.csv', help="Provide the name of the CSV File that contains the Unit Numbers and associated IMSI.")
    return parser





if __name__ == '__main__':

    parser = cli_parser()
    args = parser.parse_args()
    unit = args.unit_number
    api_key = args.API_Key
    csv_file = args.File_Name

    unit_check('LB_Deployed.csv', unit, api_key)


