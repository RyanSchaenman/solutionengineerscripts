# TODO:
# Create a script that automates the enablement of eSIM download, disabling of eSIM download and updating of SIM Status
# In the next version of this script, we can look to pull all of the information for the SIMs directly from the Zip
# file link that is produced from the Network Provisioning Details API


import http.client
import os
import zipfile
from urllib.request import urlopen
from subprocess import Popen
from os import listdir, remove
import time



# This API will obtain the network provisioning details for a specific order
def get_networkprovisioning_info(order_id, api_key):
    conn = http.client.HTTPConnection("api.alefedge.com")

    headers = { 'accept': "application/json"}

    request_string = "/connectivity/v1/order/" + order_id + "/provision?Authorization=" + api_key

    conn.request("GET", request_string, headers=headers)

    res = conn.getresponse()
    data = res.read()

    to_return = data.decode("utf-8")

    print(to_return)
    return to_return

# This API will parse the response of the network provisioning information to obtain the eSIM Zip file link
def get_eSIM_link(network_provisioning_info):
    split_response = network_provisioning_info.split('"')
    return split_response[9]


def replace_URl(eSIM_url):
    to_return = eSIM_url.split(".")
    print(to_return)
    to_return[1] = 'alefedge'
    new_string = ''
    for item in to_return:
        new_string += item
        new_string += '.'

    return new_string.strip(".")

# Download Zip file to local machine
def download_unzip(eSIM_link, password):
    Popen(['curl', '-O', eSIM_link])
    time.sleep(2)
    file_name = eSIM_link.split("/")[-1]
    Popen(['unzip','-P', password, file_name])
    time.sleep(2)
    to_return = []
    for line in listdir():
        if '.png' in line:
           to_return.append(line[:-4])
           remove(line)

    remove(file_name)
    return to_return





# This API will enable the eSIM download for all specified eSIMs
def enable_eSIM_download(eSIM_list, api_key):
    for eSIM in eSIM_list:
        conn = http.client.HTTPConnection("api.alefedge.com")

        headers = {'accept': "application/json"}

        request_string = "/connectivity/v1/sim/enable?apikey=" + api_key + "&msisdn=" + eSIM

        conn.request("PUT", request_string, headers=headers)

        res = conn.getresponse()
        data = res.read()

        print(data.decode("utf-8"))

# This API will disable the eSIM download for all specified eSIMs
def disabled_eSIM_download(eSIM_list, api_key):
    for eSIM in eSIM_list:
        conn = http.client.HTTPConnection("api.alefedge.com")

        headers = {'accept': "application/json"}

        request_string = "/connectivity/v1/sim/disable?apikey=" + api_key + "&msisdn=" + eSIM

        conn.request("PUT", request_string, headers=headers)

        res = conn.getresponse()
        data = res.read()

        print(data.decode("utf-8"))



if __name__ == '__main__':

    order_id = '633356bf7c213958c515272f'

    api_key = 'vKGyLPgfkrGO2LTIH5xVKnVPTrnLgxVj'

    eSIM_password = "63336333"

    provisioning_info = get_networkprovisioning_info(order_id, api_key)

    eSIM_Link = get_eSIM_link(provisioning_info)

    eSIM_Link = replace_URl(eSIM_Link)

    eSIM_Link = 'https://esim.alefedge.com/QR-code/hcj7oidzuj.zip'

    eSim_List = download_unzip(eSIM_Link, eSIM_password)

    print(eSim_List)

    enable_eSIM_download(eSim_List, api_key)

